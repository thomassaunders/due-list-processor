﻿/* Copyright (C) 2018 Thomas Saunders
 * This program comes with ABSOLUTELY NO WARRANTY; for details see License.txt.
 * This is free software, and you are welcome to redistribute it
 * under certain conditions as outlined in the GNU GPLv3, a copy of which is in Licence.txt.
 */

using System;
using System.IO;
using System.Net;
using System.Diagnostics;

namespace Airline_CSV_Handler
{
    class Program
    {
        // Constants for storing where various data points are within the Config array
        const int FileURL = 0;
        const int IgnoreCallsigns = 1;
        const int ICAO = 2;
        const int Delete = 3;
        const int ACTypeInclude = 4;

        // Config array, read in from program.conf
        static string[] Config = File.ReadAllLines("program.conf");

        // Variables to hold the program data
        const string ProgFileName = "Due_List_Generator.exe";
        const double ProgVersion = 1.2;

        static void Main(string[] args)
        {
            // Call functions for starting the program
            Setup();
            PrepareUpdate();
            Menu();

            
            string[] CallsignsIgnore = Config[IgnoreCallsigns].Split(','); // Array for blacklisted airlines
            string[] ACInclude = Config[ACTypeInclude].Split(','); // Array for whitelisted aircraft types (todo)

            // Read the raw data from the data file in preperation for processing
            string[] RawData = File.ReadAllLines(Config[FileURL]);

            // Generate a CSV of the raw data for easier processing later on
            GenerateCSV(RawData);

            // Create a 2D array for holding the split data for creating the duelist
            string[][] SplitData = new string[RawData.Length][];

            // Temp array for holding any data that has has a length less than 5 if it was split in the traditional way
            string[] TempArray;
            int counter = 0;

            // Split the Raw Data from a CSV into a 2D array
            for (int i = 0; i < SplitData.Length; i++)
            {
                // If splitting the data down will be a length of 5, split the data into the array
                if (RawData[i].Split(',').Length == 5)
                {
                    SplitData[i] = RawData[i].Split(',');
                }
                else // If the data is anything other than 5
                {
                    // Make the SplitData array area 5 index long for the data
                    SplitData[i] = new string[5];

                    // Split the data into the temp array
                    TempArray = RawData[i].Split(',');

                    // Fill the SplitData array
                    foreach (var item in TempArray)
                    {
                        SplitData[i][counter] = item;
                        counter += 1;
                    }

                    counter = 0;
                }
                

            }

            // Filter the results to remove unwanted airlines
            SplitData = FilterResults(SplitData, CallsignsIgnore, ACInclude);

            // Generate the due list from the data
            GenerateDueList(SplitData);

            // If the config wants the CSV to be deleted, remove the CSV
            if (Config[Delete].ToLower() == "true")
                File.Delete("DataCSV.csv");

            Console.WriteLine("Due list created.");

            // Give the user a second to read the message then exit the program
            System.Threading.Thread.Sleep(1000);
            Environment.Exit(0);
        }

        static void GenerateCSV(string[] RawData)
        {
            for (int i = 0; i < RawData.Length; i++)
            {
                // Replace tabs with commas
                RawData[i] = RawData[i].Replace('\t', ',');

                // Check if the first character is a comma, and if it is remove it
                if (RawData[i][0] == ',')
                {
                    RawData[i] = RawData[i].TrimStart(',');
                }
                // Check if the last character is a comma, and if it is remove it
                if (RawData[i][RawData[i].Length - 1] == ',')
                {
                    RawData[i] = RawData[i].TrimEnd(',');
                }
            }

            // Output the CSV file
            using (StreamWriter sw = File.AppendText("DataCSV.csv"))
            {
                foreach (var item in RawData)
                {
                    sw.WriteLine(item);
                }
            }
        }

        static string[][] FilterResults(string[][] SplitData, string[] CallsignsIgnore, string[] ACInclude)
        {
            for (int i = 0; i < SplitData.Length; i++)
            {
                // If the reg field is empty, add a note to say No Reg.
                if (SplitData[i][4] == "")
                {
                    SplitData[i][4] = "NO REG";
                }

                // Remove and entries that have a blacklisted callsign by wipeing the entry in the array
                foreach (var cs in CallsignsIgnore)
                {
                    if (SplitData[i][2].Substring(0, 3).ToUpper() == cs.ToUpper())
                    {
                        SplitData[i] = new string[0];
                        break;
                    }
                }
            }
            return SplitData;
        }

        static void GenerateDueList(string[][] SplitData)
        {
            // Output to DueList.txt
            using (StreamWriter sw = File.AppendText("DueList.txt"))
            {
                // Header line to layout the table
                sw.WriteLine("CS - Time - Reg - Type - In/Out");
                for (int i = 0; i < SplitData.Length - 1; i++)
                {
                    if (SplitData[i].Length != 0)
                    {
                        sw.WriteLine("{0} - {1} - {2} - {3} - {4}", SplitData[i][2], SplitData[i][0], SplitData[i][4], SplitData[i][3], SplitData[i][1]);
                    }
                }
            }
        }

        static void DownloadFile(string Location, string FileName = "program.conf")
        {
            WebClient wb = new WebClient();
            wb.DownloadFile(Location, FileName);
        }

        static void Menu()
        {
            string MenuInput;
            while (true)
            {
                Console.WriteLine("Please select the location you would like to generate a due list for:");
                Console.WriteLine("1. STN");
                Console.WriteLine("2. LTN");
                Console.WriteLine("3. AMS");
                Console.WriteLine("4. Exit");

                MenuInput = Console.ReadLine();

                if (MenuInput == "1")
                {
                    DownloadFile("https://bitbucket.org/thomassaunders/due-list-processor/raw/config_files/programSTN.conf");
                    break;
                }
                else if (MenuInput == "2")
                {
                    DownloadFile("https://bitbucket.org/thomassaunders/due-list-processor/raw/config_files/programLTN.conf");
                    break;
                }
                else if (MenuInput == "3")
                {
                    DownloadFile("https://bitbucket.org/thomassaunders/due-list-processor/raw/config_files/programAMS.conf");
                    break;
                }
                else if (MenuInput == "4")
                {
                    Environment.Exit(0);
                }
                else
                {
                    Console.WriteLine("Option not valid.  Please use an option number.");
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                }
            }
        }

        static void Setup()
        {
            Console.Title = "Due List Generator v" + ProgVersion.ToString();

            File.Delete("DueList.txt");
            File.Delete("DataCSV.csv");
        }

        static void PrepareUpdate()
        {
            DownloadFile("https://bitbucket.org/thomassaunders/due-list-processor/raw/config_files/version", "version");

            string Version = File.ReadAllText("version");

            double DLVersion = double.Parse(Version);

            if (DLVersion > ProgVersion)
            {
                DownloadFile("https://bitbucket.org/thomassaunders/due-list-processor/raw/config_files/changelog", "changelog");
                string[] changelog = File.ReadAllLines("changelog");

                Console.WriteLine("New Update Available! \n");

                foreach (var change in changelog)
                {
                    Console.WriteLine(change);
                }

                Console.WriteLine("\n Would you like to update? (y/n)");
                string Update = Console.ReadLine();

                if (Update.ToLower() == "y")
                {
                    UpdateProgram();
                }
                else
                {
                    Console.Clear();
                }
            }
            else
            {
                File.Delete("version");
                File.Delete("changelog");
            }

            void UpdateProgram()
            {
                string UpdateFile = "update.bat";

                DownloadFile("https://bitbucket.org/thomassaunders/due-list-processor/raw/config_files/download_location", "dllocation");

                string DownloadLocation = File.ReadAllText("dllocation");

                using (StreamWriter sw = File.CreateText(UpdateFile))
                {
                    sw.WriteLine("@echo off");
                    sw.WriteLine("del {0}", ProgFileName);
                    sw.WriteLine("powershell -command \" & { iwr " + DownloadLocation + " -OutFile " + ProgFileName + " }\"");        
                    sw.WriteLine("del {0}", "changelog");
                    sw.WriteLine("del {0}", "dllocation");
                    sw.WriteLine("del {0}", "version");
                    sw.WriteLine("echo Upload Completed. Please relaunch the program.");
                    sw.WriteLine("TIMEOUT 5");
                    sw.WriteLine("del {0}", UpdateFile);
                }

                Process.Start(UpdateFile);
                Environment.Exit(1);
            }
        }

    }
}
